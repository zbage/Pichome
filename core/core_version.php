<?php
/*
 * @copyright   QiaoQiaoShiDai Internet Technology(Shanghai)Co.,Ltd
 * @license     https://www.oaooa.com/licenses/
 * 
 * @link        https://www.oaooa.com
 * @author      zyx(zyx@oaooa.com)
 */
if(!defined('IN_OAOOA')) {
	exit('Access Denied');
}

if(!defined('CORE_VERSION')) {
	define('CORE_VERSION', 'beta3.3');
	define('CORE_VERSION_LEVEL', 'Pichome');
	define('CORE_RELEASE', '20220224');
	define('CORE_FIXBUG' , '03300000');
}